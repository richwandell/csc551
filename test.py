import numpy as np
import pandas as pd
from PIL import Image
from keras.models import load_model
from sklearn import metrics
from tqdm import tqdm

df = pd.read_csv("data/fashion-mnist_test.csv")
X = df.drop(['label'], axis=1).as_matrix()
X = np.dstack((X, X, X))
y = np.array(df[df.columns[0]], dtype=float)


def model_generator():
    for which in ['0_1', '1_1', '2_1', '0_5', '1_5', '2_5', '2_10']:
        yield (which, load_model('trained/model' + which + '.h5'))


def get_predictable(data_num):
    x_data = np.reshape(X[data_num], (28, 28, 3))
    image = Image.fromarray(x_data.astype('uint8'))
    image = image.resize((150, 150))
    image = np.array(image)
    return image


images = []
for data_num in tqdm(range(len(X))):
    images.append(get_predictable(data_num))
images = np.array(images)

for model_name, model in model_generator():
    predictions = model.predict(images)
    argmax = np.argmax(predictions, axis=1)
    with open("report/" + model_name + ".txt", "w") as f:
        f.write(metrics.classification_report(y, argmax))
        f.write(str(metrics.confusion_matrix(y, argmax)))
