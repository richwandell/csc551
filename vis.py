from PIL import Image
from keras.engine import Model
from keras.layers import Conv2D
from keras.models import load_model
from keras import backend as K
import numpy as np
import matplotlib.pyplot as plt
from fashion_mnist.fashion_data_generator import temp_image_data_folder
from scipy.misc import imsave

img_width = 150
img_height = 150
step = 1.

def deprocess_image(x):
    # normalize tensor: center on 0., ensure std is 0.1
    x -= x.mean()
    x /= (x.std() + 1e-5)
    x *= 0.1

    # clip to [0, 1]
    x += 0.5
    x = np.clip(x, 0, 1)

    # convert to RGB array
    x *= 255
    if K.image_data_format() == 'channels_first':
        x = x.transpose((1, 2, 0))
    x = np.clip(x, 0, 255).astype('uint8')
    return x


def model_generator():
    for which in ['0_1', '1_1', '2_1', '0_5', '1_5', '2_5', '2_10']:
        yield (which, load_model('trained/model' + which + '.h5'))


def keras_filters():
    test_im = Image.open(temp_image_data_folder + "\\valid\\0\\image-5.png")

    for which, model in model_generator():
        input_img = model.input
        layer_outputs = [layer.output for layer in model.layers if isinstance(layer, Conv2D)][:5]

        for filter_index, layer_output in enumerate(layer_outputs):
            loss = K.mean(layer_output[:, :, :, 0])
            grads = K.gradients(loss, input_img)[0]
            grads /= (K.sqrt(K.mean(K.square(grads))) + 1e-5)
            iterate = K.function([input_img], [loss, grads])
            input_img_data = np.copy(np.asarray(test_im, dtype='float32').reshape(1, 150, 150, 3))
            for i in range(20):
                try:
                    loss_value, grads_value = iterate([input_img_data])
                    input_img_data += grads_value * step
                except Exception as e:
                    print(e)
                    pass

            img = input_img_data[0]
            img = deprocess_image(img)
            imsave('saved_figures/%s_filter_%d.png' % (which, filter_index), img)


def just_activations():
    for which, model in model_generator():

        layer_outputs = [layer.output for layer in model.layers if isinstance(layer, Conv2D)]

        test_im = Image.open(temp_image_data_folder + "\\valid\\0\\image-5.png")
        activation_model = Model(inputs=model.input, outputs=layer_outputs)
        test_im = np.asarray(test_im).reshape(1, 150, 150, 3)
        activations = activation_model.predict(test_im)

        fig = plt.figure()
        row = 1
        for i, layer_activation in enumerate(activations):
            if i % 10 == 0 and i > 0:
                row += 1
                fig.savefig('saved_figures/' + str(which) + '_' + str(row) + '.png')
                plt.close(fig)
                fig = plt.figure()
            image_data = layer_activation[0, :, :, 0:3]
            # image_data = np.dstack((image_data, image_data, image_data))

            fig.add_subplot(row * 5, 5, i + 1)
            plt.imshow(image_data, aspect='auto')


if __name__ == "__main__":
    keras_filters()