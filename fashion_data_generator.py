import pandas as pd
import os, errno
import numpy as np
from PIL import Image

temp_image_data_folder = 'F:\\mnist\\temp_image_data'

if __name__ == "__main__":
    df = pd.read_csv("data/fashion-mnist_train.csv")
    categories = df[df.columns[0]].unique()

    train_size = int(len(df) * .75)

    x_train = df.loc[:train_size].drop(['label'], axis=1).as_matrix()
    x_train = np.dstack((x_train, x_train, x_train))
    x_valid = df.loc[train_size:].drop(['label'], axis=1).as_matrix()
    x_valid = np.dstack((x_valid, x_valid, x_valid))

    y_train = df.loc[:train_size][df.columns[0]].as_matrix()
    y_valid = df.loc[train_size:][df.columns[0]].as_matrix()

    for i, x_data in enumerate(x_train):

        directory = temp_image_data_folder + "\\train\\%d" % y_train[i]
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        x_data = np.reshape(x_data, (28, 28, 3))
        image = Image.fromarray(x_data.astype('uint8'))
        image = image.resize((150, 150))
        image.save(directory + "\\image-%d.png" % i)

    for i, x_data in enumerate(x_valid):
        directory = temp_image_data_folder + "\\valid\\%d" % y_valid[i]
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        x_data = np.reshape(x_data, (28, 28, 3))
        image = Image.fromarray(x_data.astype('uint8'))
        image = image.resize((150, 150))
        image.save(directory + "\\image-%d.png" % i)