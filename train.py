from keras.callbacks import TensorBoard
from keras.preprocessing.image import ImageDataGenerator
from fashion_mnist.fashion_data_generator import temp_image_data_folder
from fashion_mnist.models import *

epochs = 10
for i, model in [get_model_3()]:
    train_dg = ImageDataGenerator()
    train_flow = train_dg.flow_from_directory(temp_image_data_folder + "\\train", target_size=(150, 150))

    valid_dg = ImageDataGenerator()
    valid_flow = valid_dg.flow_from_directory(temp_image_data_folder + "\\valid", target_size=(150, 150))
    board = TensorBoard(log_dir='tensorboard_data/model' + str(i), write_grads=True, write_images=True)
    model.fit_generator(train_flow, 300, epochs=epochs, verbose=1, validation_data=valid_flow, validation_steps=100,
                        callbacks=[board])

    model.save('trained/model' + str(i) + '_' + str(epochs) + '.h5', include_optimizer=False)


